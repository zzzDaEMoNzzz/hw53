import React from 'react';
import './Task.css';

const Task = props => (
  <div className="Task">
    <span>{props.text}</span>
    <button onClick={props.btnHandler}>X</button>
  </div>
);

export default Task;