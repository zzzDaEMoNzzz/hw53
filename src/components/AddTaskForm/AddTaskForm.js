import React from 'react';
import './AddTaskForm.css';

const AddTaskForm = props => (
  <form className="AddTaskForm" onSubmit={props.onSubmitHandler}>
    <input type="text" value={props.inputValue} onChange={props.onChangeHandler} required />
    <button type="submit">Add</button>
  </form>
);

export default AddTaskForm;