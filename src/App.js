import React, { Component } from 'react';
import './App.css';

import AddTaskForm from './components/AddTaskForm/AddTaskForm';
import Task from './components/Task/Task';

class App extends Component {
  state = {
    tasks: [
      {id: 1545307850385, text: 'Buy milk'},
      {id: 1545310557581, text: 'Do homework'}
    ],
    taskInputValue: ''
  };

  addTask = (event) => {
    event.preventDefault();

    const tasks = [...this.state.tasks];
    tasks.push({
      id: new Date().getTime(),
      text: this.state.taskInputValue
    });

    this.setState({tasks});
    this.setState({taskInputValue: ''});
  };

  inputValueChangeHandler = event => {
    this.setState({taskInputValue: event.target.value});
  };

  deleteTask = id => {
    const tasks = [...this.state.tasks];
    const taskIndex = tasks.findIndex(task => task.id === id);

    if (taskIndex >= 0) {
      tasks.splice(taskIndex, 1);
      this.setState({tasks});
    }
  };

  render() {
    let tasks = this.state.tasks.map(task => (
      <Task
        key={task.id}
        text={task.text}
        btnHandler={() => this.deleteTask(task.id)}
      />
    ));

    return (
      <div className="container">
        <AddTaskForm
          onChangeHandler={event => this.inputValueChangeHandler(event)}
          onSubmitHandler={event => this.addTask(event)}
          inputValue={this.state.taskInputValue}
        />
        {tasks}
      </div>
    );
  }
}

export default App;
